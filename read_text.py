"""
Manish Nair & Zoe Roecker
Text Manipulation
"""
import re

def read_text(file):
    with open(file) as f:
        ent_text = f.readlines()
        print(ent_text[0].rstrip("\n"))


def count_men(file):
    count = 0
    with open(file) as f:
        for line in f.readlines():
            checker = re.compile(r"\bmen\b", flags = re.IGNORECASE)
            #print(checker.findall(line))
            count+=len(checker.findall(line))
    print(count)

def contains_blue_devil(file):
    flag = False
    with open(file) as f:
        for line in f.readlines():
            if "Duke" in line:
                print("Yes!")
                flag = True
                break
        if flag is False:
            print("No!")

def find_non_terminal_said(file):
    with open(file) as f:
        count = 0
        for line in f.readlines():
            checker = re.compile(r"\bsaid[^\.]", flags = re.IGNORECASE)
            #print(checker.findall(line))
            count+=len(checker.findall(line))
    print(count)

def cap_multi_vowel_words(file):
    with open(file) as f:
        for line in f.readlines():
            checker = re.compile(r"[aeiou][aeiou]", flags = re.IGNORECASE)
            print(checker.findall(line))

if __name__ == "__main__":
    #read_text("example_text.txt")
    #count_men("example_text.txt")
    #contains_blue_devil("example_text.txt")
    find_non_terminal_said("example_text.txt")